using Microsoft.AspNetCore.Mvc.Testing;
using System;
using Projects.API;
using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using Projects.Modelling.Entities;
using Projects.API.ExtensionsConfiguration;
using System.Linq;
using System.Text.Json;
using System.Text;
using Projects.Modelling.DTOs;
using Projects.Modelling;
using System.Collections.Generic;
using Xunit.Abstractions;

namespace Projects.API.IntegrationTesting
{
    [Collection("sync")]
    public class PostBehavioursIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper loghelper;
        private HttpClient _client;

        public PostBehavioursIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper loghelper)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            SeedData.Init();

            this.loghelper = loghelper;
        }

        [Theory]
        [InlineData("UUser","users",typeof(User))]
        [InlineData("UTeam","teams",typeof(Team))]
        [InlineData("UTask","tasks",typeof(Modelling.DTOs.Task))]
        [InlineData("UProject", "projects", typeof(Project))]
        public async System.Threading.Tasks.Task Post_WhenHasNoDependencies_HasCreated201TheSameNameAndIdGreaterThan0(string propertyName, string endpoint, Type modelType)
        {
            //Arrange

            var model = typeof(SeedData)
                 .GetProperty(propertyName, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                 .GetValue(null);

            string jsonValue = JsonSerializer.Serialize(value: model, inputType: modelType);

            //Act

            var response = await
            _client.PostAsync($"{_client.BaseAddress}api/{endpoint}", new StringContent(jsonValue, encoding: Encoding.UTF8, "application/json"));

            var returnedValue = JsonSerializer.Deserialize(
                await response.Content.ReadAsStringAsync(), returnType: modelType) as ModelBase;

            //Assert

            Assert.Equal(response.StatusCode, System.Net.HttpStatusCode.Created);

            Assert.Equal(model.GetType().GetProperty("Name").GetValue(model), returnedValue.Name);

            Assert.True(returnedValue.Id > 0);
        }
    }
}
    