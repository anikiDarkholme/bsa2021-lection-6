﻿using Microsoft.AspNetCore.Mvc.Testing;
using Projects.API.ExtensionsConfiguration;
using Projects.Modelling;
using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Xunit;
using Xunit.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Projects.Modelling.Entities;
using System.Linq;

namespace Projects.API.IntegrationTesting
{
    [Collection("sync")]
    public class DeleteBehavioursIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper loghelper;
        private HttpClient _client;
        
        public DeleteBehavioursIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper loghelper)
        {
            _client = factory
                .WithWebHostBuilder(config =>
                {
                })
                .CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            SeedData.Init();

            this.loghelper = loghelper;
        }

        [Theory]
        [InlineData("UUser", "users", typeof(User))]
        [InlineData("UTeam", "teams", typeof(Team))]
        [InlineData("UTask", "tasks", typeof(Modelling.DTOs.Task))]
        [InlineData("UProject", "projects", typeof(Project))]
        public async System.Threading.Tasks.Task Delete_WhenEntityExists_ReturnsDeleted(string propertyName, string endpoint, Type modelType)
        {
            //Arrange

            var model = typeof(SeedData)
                 .GetProperty(propertyName, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                 .GetValue(null);

            string jsonValue = JsonSerializer.Serialize(value: model, inputType: modelType);

            //Act

            var postResponse = await
            _client.PostAsync($"{_client.BaseAddress}api/{endpoint}", new StringContent(jsonValue, encoding: Encoding.UTF8, "application/json"));

            var returnedValue = JsonSerializer.Deserialize(
                await postResponse.Content.ReadAsStringAsync(), returnType: modelType) as ModelBase;

            var delResponse = await
            _client.DeleteAsync($"{_client.BaseAddress}api/{endpoint}/{returnedValue.Id}");

            //Assert

            Assert.Equal(delResponse.StatusCode, System.Net.HttpStatusCode.NoContent);

            Assert.True(returnedValue.Id > 0);
        }

        [Theory]
        [InlineData(100)]
        [InlineData(10)]
        [InlineData(50)]
        public async System.Threading.Tasks.Task Delete_WhenAddingNTeamsAndDeleteThemInParallelAsync_ThenNoContentResponseNumberIsValidAndDbIsEmpty(int teamAmount)
        {
            //Arrange 

            Team postedTeam = new Team();

            string teamJson = String.Empty;

            for (int teamNumber = 1; teamNumber <= teamAmount; teamNumber++)
            {
                postedTeam.Name = $"Team #{teamNumber}";

                teamJson = JsonSerializer.Serialize(postedTeam);

                await
               _client.PostAsync($"{_client.BaseAddress}api/teams", new StringContent(teamJson, encoding: Encoding.UTF8, "application/json"));
            }

            //Act

            var deleteTasks = new List<System.Threading.Tasks.Task<HttpResponseMessage>>();

            for (int deleteTaskNumber = 1; deleteTaskNumber <= teamAmount; deleteTaskNumber++)
            {
                deleteTasks.Add(
                    _client.DeleteAsync($"api/teams/{deleteTaskNumber}"));
            }

            await System.Threading.Tasks.Task.WhenAll(deleteTasks);

            var getResponseBody = await _client.GetStringAsync("api/teams");

            var teams = JsonSerializer.Deserialize<IEnumerable<Team>>(getResponseBody,
                new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.Preserve });

            //Assert        

            Assert.Empty(teams);

            foreach(var deleteTask in deleteTasks)
            {
                Assert.True(deleteTask.IsCompletedSuccessfully);

                Assert.Equal(deleteTask.Result.StatusCode, System.Net.HttpStatusCode.NoContent);
            }
        }
    }
}
