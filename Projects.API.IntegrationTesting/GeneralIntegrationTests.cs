﻿using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Projects.API.IntegrationTesting
{
    public class GeneralIntegrationTests :IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper loghelper;
        private HttpClient _client;

        public GeneralIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper loghelper)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            this.loghelper = loghelper;
        }

        [Theory]
        [InlineData("/api/tasks")]
        [InlineData("/api/users")]
        [InlineData("/api/teams")]
        [InlineData("/api/projects")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            // Arrange

            // Act

            var response = await _client.GetAsync(url);

            // Assert

            response.EnsureSuccessStatusCode(); 
            Assert.Equal("application/json; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }
    }
}
