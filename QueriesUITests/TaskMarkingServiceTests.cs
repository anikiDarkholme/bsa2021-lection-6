using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

using FakeItEasy;

using Projects.DataAccess.Interfaces;
using Projects.Modelling.DTOs;

using QueriesUI.Services;

using Xunit;

namespace QueriesUITests
{
    public class TaskMarkingServiceTests
    {
        private System.Timers.Timer _timer;
        private Random _randomProvider;
        private IRepository<Projects.Modelling.DTOs.Task> _taskRepo;
        private TaskCompletionService _completionService;
        private List<Projects.Modelling.DTOs.Task> _tasks;

        public TaskMarkingServiceTests()
        {
            _tasks = new List<Projects.Modelling.DTOs.Task>();
            _timer = new System.Timers.Timer(1000);
            _randomProvider = new Random();
            _taskRepo = A.Fake<IRepository<Projects.Modelling.DTOs.Task>>();
            _completionService = new TaskCompletionService(_timer, _taskRepo, _randomProvider);
        }

        [Fact]
        public async System.Threading.Tasks.Task MarkRandomTaskWithCompleted_WhenRuns_DoesntAffectCollectionCountAndChangesOneId()
        {
            //Arrange

            A.CallTo(() => _taskRepo.GetAllAsync())
                .Returns(_tasks);

            A.CallTo(() => _taskRepo.DeleteAtAsync(A<int>.Ignored))
                .Invokes((int id) => _tasks.RemoveAll(n => n.Id == id));

            A.CallTo(() => _taskRepo.AddAsync(A<Projects.Modelling.DTOs.Task>.Ignored))
                .ReturnsLazily((Projects.Modelling.DTOs.Task task) =>
                {
                    _tasks.Add(task);

                    return task;
                });


            _tasks.AddRange(new List<Projects.Modelling.DTOs.Task>
            {
              new Projects.Modelling.DTOs.Task()
              {
                  Name = "Name1",
                  Id = 1,
                  State = State.InProgress
              },
              new Projects.Modelling.DTOs.Task()
              {
                  Name = "Name2",
                  Id = 2,
                  State = State.InProgress
              }
            });

            var tokenCancellationSource = new CancellationTokenSource();

            //Act

            int markedID = await
            _completionService.MarkRandomTaskWithDelay(1000, tokenCancellationSource.Token);

            //Assert

            Assert.Equal(_tasks.Count, 2);

            Assert.Equal(_tasks.Single(n => n.Id == markedID).State, State.Finished);
        }
    }
}
