﻿using System.Threading.Tasks;

namespace Projects.QueriesUI.Interfaces
{
    interface IApplicationInterface
    {
        Task RunAsync();
    }
}