﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.QueriesUI
{
    internal class UI
    {
        private readonly int taskAmount;
        private readonly string[] tasks;
        private readonly string[] taskNames;

        public UI(IEnumerable<string> tasks, IEnumerable<string> taskNames)
        {
            if (tasks.Count() != taskNames.Count()) throw new Exception("taskNames length != tasks length");

            this.taskAmount = taskNames.Count();
            this.tasks = tasks.ToArray();
            this.taskNames = taskNames.ToArray();
        }

        internal string GetTaskInfo()
        {
            DrawInfoMenu();

            var taskRes = KeyLoop();

            Console.Clear();

            return tasks[taskRes - 1];
        }

        private int KeyLoop()
        {
            Console.CursorVisible = false;

            int loopY = 0;

            while (true)
            {
                DrawMenuSelected(loopY);

                try {
                    loopY += GetCursorYChange(loopY);
                }
                catch(Exception ex) {
                    return Convert.ToInt32(ex.Message);
                }
            }
        }

        private int GetCursorYChange(int currentY)
        {
            switch (Console.ReadKey(intercept: true).Key)
            {
                case ConsoleKey.DownArrow:
                    if (currentY < taskAmount - 1) return 1;
                    break;

                case ConsoleKey.UpArrow:
                    if (currentY > 0) return -1;
                    break;

                case ConsoleKey.Enter:
                    {
                        throw new Exception($"{currentY + 1}");
                    }

                default: 
                    return 0;
            }
            return 0;
        }
        private void DrawMenuSelected(int selectedY)
        {
            int nativeLeft = Console.CursorLeft,
                nativeTop = Console.CursorTop;

            Console.SetCursorPosition(0, 0);

            for (int i = 0; i < taskAmount; i++)
            {
                if (Console.CursorTop == selectedY)
                    Console.ForegroundColor = ConsoleColor.Blue;

                Console.WriteLine(taskNames[i]);

                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.SetCursorPosition(nativeLeft, nativeTop);
        }
        private static void DrawInfoMenu()
        {
            int initialX = Console.CursorLeft;
            int initialY = Console.CursorTop;

            Console.SetCursorPosition(0, 7);
            Console.WriteLine("Use arrows to navigate the menu. Enter to choose the task \n" +
                              "On adding elements, give 0 as an integer parameter to omit it, \n" +
                              "if handling a request with unspecified foreign key. \n" +
                              "Also menu system is done purely, so to exit nth invoked menu, \n" +
                              "after the operation and only after the operation press ESC n times!");

            Console.SetCursorPosition(initialX, initialY);
        }
    }
}