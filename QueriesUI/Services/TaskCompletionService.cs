﻿using Projects.DataAccess.Interfaces;

using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace QueriesUI.Services
{
    public class TaskCompletionService
    {
        private readonly System.Timers.Timer _evaluationTimer;
        private readonly IRepository<Projects.Modelling.DTOs.Task> _taskRepo;
        private readonly Random _randomProvider;

        public TaskCompletionService(System.Timers.Timer evaluationTimer,
                                     IRepository<Projects.Modelling.DTOs.Task> taskRepo,
                                     Random randomProvider)
        {
            this._evaluationTimer = evaluationTimer;
            this._taskRepo = taskRepo;
            this._randomProvider = randomProvider;
        }

        public Task<int> MarkRandomTaskWithDelay(int delay, CancellationToken token)
        {
            Console.WriteLine("Task Marking...");

            var tcs = new TaskCompletionSource<int>();

            var stopwatch = new Stopwatch();

            stopwatch.Start();

            Projects.Modelling.DTOs.Task randomTask = new Projects.Modelling.DTOs.Task();

            _evaluationTimer.Elapsed += async (sender, e) =>
            {
                try
                {
                    var deletionTask = _taskRepo.DeleteAtAsync(randomTask.Id);

                    randomTask.State = Projects.Modelling.DTOs.State.Finished;

                    var addingTask = _taskRepo.AddAsync(randomTask);

                    await Task.WhenAll(new Task[] { deletionTask, addingTask });

                    tcs.TrySetResult(addingTask.Result.Id);
                }
                catch (Exception ex)
                {
                    tcs.TrySetException(ex);
                }
            };

            try
            {
                 randomTask = GetRandomTask(delay, stopwatch, token).GetAwaiter().GetResult();
            }
            catch { throw; }
            finally
            {
                stopwatch.Stop();
            }

            Debug.WriteLine($"Elapsed on Interval count: {stopwatch.ElapsedMilliseconds}");

            _evaluationTimer.Interval = delay - stopwatch.ElapsedMilliseconds;

            _evaluationTimer.AutoReset = false;

            _evaluationTimer.Start();

            return tcs.Task;
        }

        private async Task<Projects.Modelling.DTOs.Task> GetRandomTask(int delay, Stopwatch stopwatch, CancellationToken token)
        {
            Debug.WriteLine($"Elapsed on GetRandomTask: {stopwatch.ElapsedMilliseconds}");

            List<Projects.Modelling.DTOs.Task> tasks = new List<Projects.Modelling.DTOs.Task>();

            do
            {
                try
                {
                    tasks = (await GetTasks()).ToList();
                }
                catch { }

                token.ThrowIfCancellationRequested();

                break;
            }
            while (stopwatch.ElapsedMilliseconds < delay);

            if (tasks.Count() < 1)
                throw new InvalidOperationException("Server doesn't contain any tasks");

            var randomTask =
            tasks.ElementAt(_randomProvider.Next(tasks.Count));

            if (stopwatch.ElapsedMilliseconds > delay)
                throw new TaskCanceledException("Given delay was outreached, while retrieving tasks to mark. No subsequent operations with it were started.");

            return randomTask;
        }

        private async Task<IEnumerable<Projects.Modelling.DTOs.Task>> GetTasks()
        {
            try
            {
                var tasks = await _taskRepo.GetAllAsync();

                return tasks;
            }
            catch(Exception ex) { throw new InvalidOperationException("Unable to get tasks from the server", ex); }
        }
    }
}
