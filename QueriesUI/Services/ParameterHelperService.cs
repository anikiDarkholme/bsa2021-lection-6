﻿using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueriesUI.Services
{
    public class ParameterHelperService
    {
        public T CreateModel<T>()
        {
            var modelType = typeof(T);

            var method = this.GetType().GetMethod($"Create{modelType.Name}",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

            return (T)method.Invoke(this,null);
        }

        public T GetParameter<T>(string parmaName)
        {
            Console.Clear();
            Console.WriteLine("This method accepts " + parmaName + " parameter :");

            T param;
            while (true)
            {
                try
                {
                    param = (T)Convert.ChangeType(Console.ReadLine(), typeof(T));

                    break;
                }
                catch { Console.WriteLine("Wrong parameter type"); }
            }

            return param;
        }

        public bool BasicCheckId(int userId)
        {
            if (userId < 0)
                return false;

            return true;
        }

#region private

        private Project CreateProject()
        { 
            var project = new Project()
            {
                CreatedAt = GetParameter<DateTime>("Created date"),

                Name = GetParameter<string>("Name"),

                Description = GetParameter<string>("Description"),

                AuthorId = GetParameter<int>("Author Id"),

                Deadline = GetParameter<DateTime>("Deadline"),

                TeamId = GetParameter<int>("Team ID")
            };

            if (project.AuthorId == 0)
                project.AuthorId = null;
            if (project.TeamId == 0)
                project.TeamId = null;

            return project;
        }

        private Task CreateTask()
        {
            var task = new Task()
            {
                CreatedAt = GetParameter<DateTime>("Created date"),

                Name = GetParameter<string>("Name"),

                Description = GetParameter<string>("Description"),

                FinishedAt = GetParameter<DateTime>("Finished date"),

                PerformerId = GetParameter<int>("Performer Id"),

                ProjectId = GetParameter<int>("Project Id"),

                State = (State)GetParameter<int>("State[0 | 1 | 2 | 3]  => [Created | InProgress | Finished | Canceled]"),
            };

            if (task.PerformerId== 0)
                task.PerformerId = null;
            if (task.ProjectId == 0)
                task.ProjectId = null;

            return task;
        }

        private Team CreateTeam()
        {
            var team = new Team()
            {
                CreatedAt = GetParameter<DateTime>("Created date"),

                Name = GetParameter<string>("Name"),
            };

            return team;
        }

        private User CreateUser()
        {
            var user = new User()
            {
                BirthDay= GetParameter<DateTime>("Created date"),

                FirstName = GetParameter<string>("First Name"),

                LastName = GetParameter<string>("Last Name"),

                Email = GetParameter<string>("Email"),

                RegisteredAt= GetParameter<DateTime>("Registration date"),

                TeamId= GetParameter<int>("Team Id")
            };

            if (user.TeamId== 0)
                user.TeamId= null;
            
            return user;
        }

#endregion
    }
}
