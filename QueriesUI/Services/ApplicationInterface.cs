﻿using Autofac;
using Projects.Modelling.Interfaces;
using Projects.QueriesUI.Interfaces;
using System;
using System.Threading.Tasks;

namespace Projects.QueriesUI.Services
{
    class ApplicationInterface : IApplicationInterface
    {
        private readonly IComponentContext context;

        public ApplicationInterface(IComponentContext context)
        {
            this.context = context;
        }

        public async Task RunAsync()
        {
            while (true)
            {
                var taskInfo= new UI(new[]{ "EntityService", "QueryService" } ,
                                     new[]{ "Work with entities", "Work with queries" }).GetTaskInfo();

                var type = Type.GetType("Projects.QueriesUI.Services." + taskInfo);

                var method =
                type.GetMethod("Run");

                Task runTask = (Task)method.Invoke(context.Resolve(type), null);

                await runTask;
 
                Console.WriteLine("\n Press Esc to quit. Other buttons restart the execution");
                if (Console.ReadKey().Key == ConsoleKey.Escape) break;
                Console.Clear();
            }
        }
 

    }
}
