﻿using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.Modelling;
using Projects.QueriesUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QueriesUI.Services
{
    public class InstanceWorker<T> where T : ModelBase, new()
    {
        private HttpClient _client;

        private IRepository<T> _repo;

        private readonly ParameterHelperService _parameterHelper;
        private readonly TaskCompletionService taskCompletion;

        public InstanceWorker(HttpClient client,
                              IRepository<T> repository,
                              ParameterHelperService parameterHelper,
                              TaskCompletionService taskCompletion)
        {
            _client = client;

            _repo = repository;

            this._parameterHelper = parameterHelper;
            this.taskCompletion = taskCompletion;
        }

        public async Task<string> Run()
        {
            var methods = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);

            var methodNames = GetDescriptedMethodNames(methods);

            var methodDescs = GetDescriptionValues(methods);

            while (true)
            {
                var taskInfo = new UI(methodNames, methodDescs).GetTaskInfo();

                await RunTaskOnName(taskInfo);

                Console.WriteLine("\n Press Esc to quit. Other buttons restart the execution");
                if (Console.ReadKey().Key == ConsoleKey.Escape) break;
                Console.Clear();
            }

            return "";
        }

        public async Task RunTaskOnName(string taskName)
        {
            var type = GetType();

            var method = type.GetMethod(taskName,
                BindingFlags.NonPublic | BindingFlags.Instance);

            Task task = (Task)method.Invoke(this, null);

            await task;
        }

        #region private

        #region helpers

        private static IEnumerable<string> GetDescriptionValues(MethodInfo[] methods)
        {
            return methods
                                .Where(n => n.GetCustomAttributes()
                                                 .Any(a => a.GetType() == typeof(DescriptionAttribute)))
                                .Select(n => n.GetCustomAttributesData()

                                                 .Where(a => a.AttributeType == typeof(DescriptionAttribute)))
                                .Select(n => n.Single().ConstructorArguments.Single().Value.ToString());
        }

        private static IEnumerable<string> GetDescriptedMethodNames(MethodInfo[] methods)
        {
            return methods
                                .Where(n => n.GetCustomAttributes()
                                                   .Any(a => a.GetType() == typeof(DescriptionAttribute)))
                                .Select(n => n.Name);
        }



#endregion

        [Description("Delete an element")] 
        private async System.Threading.Tasks.Task Delete()
        {

            Console.WriteLine("Running Delete on " + typeof(T));

            int Id = _parameterHelper.GetParameter<int>("Id");

            try
            {
                await
                    _repo.DeleteAtAsync(Id);
            }
            catch (Exception ex){ Console.Error.WriteLine(ex.Message); }
        }

        [Description("Replace an element")]
        private async Task Put()
        {
            Console.WriteLine("Running PUT on "+ typeof(T));

            int id = _parameterHelper.GetParameter<int>("Id");

            try 
            { 
                await _repo.DeleteAtAsync(id);
                
                var model = _parameterHelper.CreateModel<T>();

                await _repo.AddAsync(model);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Given model wasn't able to change the existing one.");

                Console.Error.WriteLine(ex.Message); 
            }
        }

        [Description("Add new element")]
        private async System.Threading.Tasks.Task Post()
        {
            Console.WriteLine("Running POST on " + typeof(T));

            var model = _parameterHelper.CreateModel<T>();

            try
            {
                await _repo.AddAsync(model);
            }
            catch (Exception ex) 
            {
                Console.Error.WriteLine("Given model wasn't added to the system for some reason!");

                Console.Error.WriteLine(ex.Message); 
            }

            if (typeof(T) == typeof(Projects.Modelling.DTOs.Task))
            {
                MarkATask();
            }
        }

        private async Task MarkATask()
        {
            var cancellationSource = new CancellationTokenSource();

            try
            {
                int markedId = await taskCompletion.MarkRandomTaskWithDelay(1000, cancellationSource.Token);

                Console.SetCursorPosition(30,0);

                Console.WriteLine($"Marked Task with Id: {markedId}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Description("Get an element by Id")]
        private async System.Threading.Tasks.Task GetById()
        {
            Console.WriteLine("Running GETByID on " + typeof(T));

            int id = _parameterHelper.GetParameter<int>("Id");

            T model = new T();

            try 
            { 
                model = await _repo.GetByIdAsync(id);

                Console.WriteLine(model.ToString());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Model with a given Id was not found for some reason!\n" + ex.Message); 
            }

        }

        [Description("Get all elements")]
        private async Task GetAll()
        {
            Console.WriteLine("Running GETALL on " + typeof(T));

            IEnumerable<T> models = new List<T>();

            try 
            { 
                models = await _repo.GetAllAsync();
            }
            catch (Exception ex) { Console.Error.WriteLine(ex.Message); }

            if(models.Count() < 1)
                Console.WriteLine("None elements found!");

            foreach (var item in models)
            {
                Console.WriteLine(item.ToString());
            }
        }

#endregion
    }

}
