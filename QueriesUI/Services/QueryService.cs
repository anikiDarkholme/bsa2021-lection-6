﻿using Projects.Modelling.Entities;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using Projects.Modelling.DTOs;
using System.Reflection;
using System.ComponentModel;
using QueriesUI.Services;

namespace Projects.QueriesUI.Services
{
    class QueryService
    {
        private readonly ParameterHelperService _parameterHelper;
        private readonly HttpClient _client;

        public QueryService(ParameterHelperService parameterHelper, HttpClient client)
        {
            this._parameterHelper = parameterHelper;
            this._client = client;
        }

       public string Run()
        {
            Console.Clear();
            while (true)
            {
                var taskInfo = new UI(
                    new[]{
                "ShowTasksQuantityPerProject",
                "ShowTasksForPerformer",
                "ShowTasksPerPerformerFromThisYear",
                "ShowOldestUsersByTeam",
                "ShowTasksPerPerformer",
                "ShowUserInfo",
                "ShowProjectsInfo"
                },
                    new[]{
                "Task 1",
                "Task 2",
                "Task 3",
                "Task 4",
                "Task 5",
                "Task 6",
                "Task 7"
                }).GetTaskInfo();

                RunTaskOnName(taskInfo);

                Console.WriteLine("\n Press Esc to quit. Other buttons restart the execution");
                if (Console.ReadKey().Key == ConsoleKey.Escape) break;
                Console.Clear();
            }

            return "";
        }

        #region helpers

        private static IEnumerable<string> GetDescriptionValues(MethodInfo[] methods)
        {
            return methods
                                .Where(n => n.GetCustomAttributes()
                                                 .Any(a => a.GetType() == typeof(DescriptionAttribute)))
                                .Select(n => n.GetCustomAttributesData()

                                                 .Where(a => a.AttributeType == typeof(DescriptionAttribute)))
                                .Select(n => n.Single().ConstructorArguments.Single().Value.ToString());
        }

        private static IEnumerable<string> GetDescriptedMethodNames(MethodInfo[] methods)
        {
            return methods
                                .Where(n => n.GetCustomAttributes()
                                                   .Any(a => a.GetType() == typeof(DescriptionAttribute)))
                                .Select(n => n.Name);
        }



        #endregion

        public async System.Threading.Tasks.Task RunTaskOnName(string taskName)
        {
            var type = GetType();

            var method = type.GetMethod(taskName,
                BindingFlags.NonPublic | BindingFlags.Instance);

            System.Threading.Tasks.Task task = (System.Threading.Tasks.Task)method.Invoke(this, null);

            await task;
        }
        
        [Description("Task 1")]
        private async System.Threading.Tasks.Task ShowTasksQuantityPerProjectAsync(int authorId)
        {
            if (!_parameterHelper.BasicCheckId(authorId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            Console.WriteLine("Tasks quantity per project for author with id = " + authorId);

            var taskQuantityByProjectstring = await 
                new HttpClient().GetStringAsync($"https://localhost:5001/api/misc/tasksQuantity/{authorId}");

            var taskQuantityByProject =
                JsonConvert.DeserializeObject<List<KeyValuePair<ProjectEntity, List<TaskEntity>>>>(taskQuantityByProjectstring);

            if (taskQuantityByProject.Count() < 1)
            {
                Console.WriteLine("No tasks were found for this author");
                return;
            }
            foreach (var item in taskQuantityByProject)
            {
                Console.WriteLine($"Project {item.Key.Name} has {item.Value.Count()} tasks");
            }
        }

        [Description("Task 2")]
        private async System.Threading.Tasks.Task ShowTasksForPerformerAsync(int performerId)
        {
            if (!_parameterHelper.BasicCheckId(performerId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            Console.WriteLine("Tasks per performer with Id = " + performerId);

            var tasksString = await
                _client.GetStringAsync($"https://localhost:5001/api/misc/tasks/{performerId}");

            var tasks = JsonConvert.DeserializeObject<List<TaskEntity>>(tasksString);

            if (tasks.Count < 1)
            {
                Console.WriteLine("No tasks with Name less than 45 chars for this performer found");
            }

            foreach (var item in tasks)
            {
                Console.WriteLine($" Task {item.Name} with Id = {item.Id}");
            }
        }

        [Description("Task 3")]
        private async System.Threading.Tasks.Task ShowTasksPerPerformerFromThisYear(int userId)
        {
            if (!_parameterHelper.BasicCheckId(userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            Console.WriteLine("Tasks per Performer finished this year. ID of performer = " + userId);

            var tasksString = await
                _client.GetStringAsync($"https://localhost:5001/api/misc/tasksThisYear/{userId}");

            var tasks = JsonConvert.DeserializeObject<List<TaskInfo>>(tasksString);

            if (tasks.Count() < 1)
            {
                Console.WriteLine("No tasks were found. Make sure your input is correct.");
                return;
            }

            foreach (var item in tasks)
            {
                Console.WriteLine($"Task #{item.Id} called {item.Name} for Performer with Id = {userId}");
            }
            Console.WriteLine("-______-");
        }

        [Description("Task 4")]
        private async System.Threading.Tasks.Task ShowOldestUsersByTeam()
        {
            Console.WriteLine("Oldest users by team");

            var tasksString = await
                _client.GetStringAsync($"https://localhost:5001/api/misc/oldestTeams");

            var tasks = JsonConvert.DeserializeObject<List<OldestUsersInfo>>(tasksString);

            if (tasks.Count() < 1)
            {
                Console.WriteLine("No results were found");
                return;
            }

            foreach (var item in tasks)
            {
                Console.WriteLine($" From team #{item.Id} named {item.Name} users:");
                foreach (var user in item.Users)
                {
                    Console.WriteLine($"User {user.FirstName}");
                }
                Console.WriteLine("-------");
            }
        }

        [Description("Task 5")]
        private async System.Threading.Tasks.Task ShowTasksPerPerformer()
        {
            Console.WriteLine("Tasks Per Performer Alphabetically");

            var tasksString = await
                _client.GetStringAsync($"https://localhost:5001/api/misc/tasksAlpha");

            var tasks = JsonConvert.DeserializeObject<List<KeyValuePair<UserEntity, List<TaskEntity>>>>(tasksString);

            if (tasks.Count() < 1)
            {
                Console.WriteLine("No results were found.");
                return;
            }

            foreach (var item in tasks)
            {
                Console.WriteLine($"---------Performer {item.Key.Id}-------------");
                Console.WriteLine($"For Performer {item.Key.FirstName} with Id {item.Key.Id} Tasks:");

                foreach (var task in item.Value)
                {
                    Console.WriteLine($"Task {task.Name} with Id {task.Id}");
                }
                Console.WriteLine("------------------------------------------------");
            }
        }

        [Description("Task 6")]
        private async System.Threading.Tasks.Task ShowUserInfo(int userId)
        {
            if (!_parameterHelper.BasicCheckId(userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            Console.WriteLine("User Info for id = " + userId);

            try
            {
                var userInfoString = await
                    _client.GetStringAsync($"https://localhost:5001/api/misc/userInfo/{userId}");

                var userInfo = JsonConvert.DeserializeObject<UserInfo>(userInfoString);

                Console.WriteLine($"User {userInfo.User.FirstName}" +
                $" has last project {userInfo.LastProject.Name} ," +
                $" {userInfo.LastProjectTasksQuantity} tasks in it, " +
                $"{userInfo.UnhandledTasksQuantity} unhandeled tasks " +
                $"and longest task {userInfo.LongetsTask.Name}");
            }
            catch
            {
                Console.WriteLine($"No projects were found for user " + userId);
            }
        }

        [Description("Task 7")]
        private async System.Threading.Tasks.Task ShowProjectsInfo()
        {
            Console.WriteLine("Projects Info");

            Console.WriteLine($"Project Name" +
                $" | Longest Task" +
                $" | Shortest Task" +
                $" | Quantity of users");

            var tasksString = await
                 _client.GetStringAsync($"https://localhost:5001/api/misc/projectsInfo");

            var tasks = JsonConvert.DeserializeObject<List<ProjectInfo>>(tasksString);

            foreach (var item in tasks)
            {
                Console.WriteLine($"{item.Project.Name}" +
                    $" | {item.LongestTask?.Name ?? "No tasks found"}" +
                    $" | {item.ShortestTask?.Name ?? "No tasks found"} " +
                    $" | {item.UsersQuantity}");
            }
        }

    }
}

