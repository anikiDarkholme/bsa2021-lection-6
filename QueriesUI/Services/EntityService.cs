﻿using Autofac;
using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.Modelling.Entities;
using Projects.QueriesUI.Interfaces;
using QueriesUI.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Projects.QueriesUI.Services
{
    class EntityService
    {
        private readonly IComponentContext context;

        public EntityService(IComponentContext context)
        {
            this.context = context;
        }
        public async Task<string> Run()
        {
            Console.Clear();

            while (true)
            {

                var taskInfo = new UI(new[] { "Task", "Team", "User", "Project" },
                                     new[] { "Work with Tasks", "Work with Teams", "Work with Users", "Work with Projects" }).GetTaskInfo();

                try
                {
                    await GetRunTask(taskInfo);
                }
                catch
                {
                    Console.WriteLine("Error!");
                    throw;
                }

                Console.WriteLine("\n Press Esc to quit. Other buttons restart the execution");
                if (Console.ReadKey().Key == ConsoleKey.Escape) break;
                Console.Clear();

            }
            return "";
        }

        private Task GetRunTask(string typeName)
        {
            Type modelType = Type.GetType($"Projects.Modelling.DTOs.{typeName}, Modelling, Version = 1.0.0.0, Culture = neutral, PublicKeyToken = null");

            Type workerType = typeof(InstanceWorker<>);

            var genericWorkerType = workerType.MakeGenericType(modelType);

            var genericInstance = context.Resolve(genericWorkerType);

            return (Task)genericWorkerType.GetMethod("Run").Invoke(genericInstance, parameters: null);
        }
    }
}

