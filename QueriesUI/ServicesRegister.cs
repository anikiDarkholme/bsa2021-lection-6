﻿using Autofac;
using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.QueriesUI.Interfaces;
using Projects.QueriesUI.Services;
using QueriesUI.Services;

using System;
using System.Net.Http;
using System.Timers;

namespace Projects.QueriesUI
{
    class ServicesRegister
    {
        public static IContainer RegisterContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();


            builder.RegisterType<HttpClient>()
                .AsSelf()
                .InstancePerDependency();

            builder.RegisterType<Timer>()
                .AsSelf()
                .WithParameter(new TypedParameter(typeof(double), 1000));

            builder.RegisterType<ApplicationInterface>()
                .As<IApplicationInterface>();

            builder.RegisterTypes(typeof(EntityService),
                                  typeof(QueryService),
                                  typeof(Random),
                                  typeof(ParameterHelperService),
                                  typeof(TaskCompletionService))
                .SingleInstance();

            builder.RegisterGeneric(typeof(InstanceWorker<>))
                .AsSelf();

            builder.RegisterGeneric(typeof(APIRepository<>))
                .As(typeof(IRepository<>))
                .WithParameter(new TypedParameter(typeof(string),"https://localhost:5001/api/"));

            return builder.Build();
        }

    }
}
