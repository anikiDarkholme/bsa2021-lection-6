﻿using Microsoft.EntityFrameworkCore;
using Projects.DataAccess.Interfaces;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.DataAccess.Repositories
{
    public class TeamRepository : DBRepository<TeamEntity>, ITeamRepository
    {
        public TeamRepository(DbContext context)
        : base(context)
        {
        }
    }
}
