﻿using Projects.DataAccess.Interfaces;
using Projects.Modelling;
using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Projects.DataAccess.Repositories
{
    public class APIRepository<T> : IRepository<T> where T : ModelBase, new()
    {
        public APIRepository(string apiEndpoint, HttpClient client)
        {
            this._apiEndpoint = apiEndpoint;

            typedEndpoint = $"{_apiEndpoint}{typeof(T).Name.ToLower()}s/";

            this._client = client;
        }

        private readonly string _apiEndpoint;
        private readonly HttpClient _client;
        private string typedEndpoint;
        
        #region public:

        public IEnumerable<T> GetAll()
        {
            Console.WriteLine("Get All method");

            var response =  _client.GetAsync(typedEndpoint).Result;

            response.EnsureSuccessStatusCode();

            var jsonModels = response.Content.ReadAsStringAsync().Result;

            return JsonSerializer.Deserialize<IEnumerable<T>>(jsonModels);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var response = await _client.GetAsync(typedEndpoint);

            response.EnsureSuccessStatusCode();

            var jsonModelsString = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<IEnumerable<T>>(jsonModelsString,
                new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.Preserve });
        }

        public T GetById(int id)
        {
            var response = _client.GetAsync(typedEndpoint + id).Result;

            response.EnsureSuccessStatusCode();

            var jsonModelstring = response.Content.ReadAsStringAsync().Result;

            return JsonSerializer.Deserialize<T>(jsonModelstring);
        }

        public async Task<T> GetByIdAsync(int id, params Expression<Func<T, object>>[] includeExpression)
        {   
            var response = await _client.GetAsync(typedEndpoint + id);

            response.EnsureSuccessStatusCode();

            var jsonModelstream = await response.Content.ReadAsStreamAsync();

            return await 
                JsonSerializer.DeserializeAsync<T>(jsonModelstream);
        }

        public T Add(T entity)
        {
            entity.Id = 0;

            var jsonString = JsonSerializer.Serialize(entity);

            var stringContent = new StringContent(jsonString, Encoding.UTF8, mediaType: "application/json");

            var response =
                _client.PostAsync(typedEndpoint, stringContent).Result;

            response.EnsureSuccessStatusCode();

            return JsonSerializer.Deserialize<T>(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<T> AddAsync(T entity)
        {
            entity.Id = 0;

            var jsonString = JsonSerializer.Serialize(entity);

            var stringContent = new StringContent(jsonString, Encoding.UTF8, mediaType : "application/json");

            var response =
                await _client.PostAsync(typedEndpoint, stringContent);

            response.EnsureSuccessStatusCode();

            return await 
                JsonSerializer.DeserializeAsync<T>(await 
                                response.Content.ReadAsStreamAsync());
        }

        public void DeleteAt(int id)
        {
            var response =
                _client.DeleteAsync(typedEndpoint).Result;

            response.EnsureSuccessStatusCode();
        }

        public async System.Threading.Tasks.Task DeleteAtAsync(int id)
        {
            var response = 
            await _client.DeleteAsync(typedEndpoint + id);

            response.EnsureSuccessStatusCode();
        }

        public int Count() => 4;

        #endregion
    }
}
