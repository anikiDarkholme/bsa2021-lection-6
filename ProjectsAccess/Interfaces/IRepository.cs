﻿using Projects.Modelling;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Projects.DataAccess.Interfaces
{
    public interface IRepository<T>
    {
        T GetById(int id);

        Task<T> GetByIdAsync(int id,
            params Expression<Func<T, object>>[] includeExpression);

        IEnumerable<T> GetAll();

        Task<IEnumerable<T>> GetAllAsync();

        T Add(T entity);

        Task<T> AddAsync(T entity);

        void DeleteAt(int id);

        Task DeleteAtAsync(int id);

        int Count();
    }
}
