﻿using System;
using System.Text.Json.Serialization;

namespace Projects.Modelling.DTOs
{ 
	public class Project : ModelBase
	{
		[JsonPropertyName("authorId")]
		public int? AuthorId { get; set; }

		[JsonPropertyName("teamId")]
		public int? TeamId { get; set; }

		[JsonPropertyName("description")]
		public string Description { get; set; }

		[JsonPropertyName("deadline")]
		public DateTime Deadline { get; set; }

		[JsonPropertyName("createdAt")]
		public DateTime CreatedAt { get; set; }

		public override string ToString()
		{
			return $"Project --------\n" +
				$"Id : {Id}|\n" +
				$" Name : {Name}|\n " +
				$"Description : {Description}|\n " +
				$"DeadLine : {Deadline}|\n Created At : {CreatedAt}|" +
				$"Author Id : {AuthorId}\n" +
				$"Team Id : {TeamId}\n" +
				$"\n";
		}
	}
}

