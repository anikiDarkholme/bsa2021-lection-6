﻿using System;
using System.Text.Json.Serialization;

namespace Projects.Modelling.DTOs
{
	public class Team : ModelBase
	{

		[JsonPropertyName("createdAt")]
		public DateTime CreatedAt { get; set; }
		public override string ToString()
		{
			return $"Team --------\n" +
				$"Id : {Id}|\n" +
				$" Name : {Name}|\n " +
				$"Created At : {CreatedAt}|\n" +
				$"\n";
		}
	}
}
