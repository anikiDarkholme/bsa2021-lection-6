﻿using System;
using System.Text.Json.Serialization;

namespace Projects.Modelling.DTOs
{
	public class User : ModelBase
	{
		[JsonPropertyName("teamId")]
		public int? TeamId { get; set; }

		[JsonPropertyName("firstName")]
		public string FirstName
		{
			get
			{
				return Name;
			}
			set
			{
				Name = value;
			}
		}

		[JsonPropertyName("lastName")]
		public string LastName { get; set; }

		[JsonPropertyName("email")]
		public string Email { get; set; }

		[JsonPropertyName("registeredAt")]
		public DateTime RegisteredAt { get; set; }

		[JsonPropertyName("birthDay")]
		public DateTime BirthDay { get; set; }

		public override string ToString()
		{
			return $"User --------\n" +
				$"Id : {Id}|\n" +
				$" First Name : {FirstName}|\n " +
				$"Last Name : {LastName}|\n " +
				$"Email: {Email}|\n" +
				$"Registered At : {RegisteredAt}|\n " +
				$"Birth Day : {BirthDay}|\n " +
				$"Team Id : {TeamId}|\n" +
				$"\n";
		}
	}
}
