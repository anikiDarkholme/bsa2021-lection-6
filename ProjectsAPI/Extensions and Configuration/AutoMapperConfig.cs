﻿using AutoMapper;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.API.ExtensionsConfiguration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<User, UserEntity>()
                .ForMember(dest => dest.TeamEntityId, opt => opt.MapFrom(src => src.TeamId))
                .ReverseMap();

            CreateMap<Modelling.DTOs.Task, TaskEntity>()
                .ForMember(dest => dest.ProjectEntityId, opt => opt.MapFrom(src => src.ProjectId))
                .ForMember(dest => dest.UserEntityId, opt => opt.MapFrom(src => src.PerformerId))
                .ReverseMap();

            CreateMap<Team, TeamEntity>()
                .ReverseMap();

            CreateMap<Project, ProjectEntity>()
                .ForMember(dest => dest.UserEntityId,opt => opt.MapFrom(src => src.AuthorId))
                .ForMember(dest => dest.TeamEntityId, opt => opt.MapFrom(src => src.TeamId))
                .ReverseMap();
        }
    }
}
