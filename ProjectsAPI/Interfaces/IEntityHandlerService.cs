﻿using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Projects.API.Interfaces
{
    public interface IEntityHandlerService
    {
        Task<IEnumerable<ProjectEntity>> GetAllProjectEntitiesAsync(CancellationToken token);
        Task<IEnumerable<TaskEntity>> GetAllTaskEntitiesAsync(CancellationToken token);
        Task<IEnumerable<TeamEntity>> GetAllTeamEntitiesAsync(CancellationToken token);
        Task<IEnumerable<UserEntity>> GetAllUserEntitiesAsync(CancellationToken token);
        Task<ProjectEntity> GetProjectEntitybyIdAsync(int id, CancellationToken token);
        Task<TaskEntity> GetTaskEntitybyIdAsync(int id, CancellationToken token);
        Task<TeamEntity> GetTeamEntitybyIdAsync(int id, CancellationToken token);
        Task<UserEntity> GetUserEntitybyIdAsync(int id, CancellationToken token);
    }
}