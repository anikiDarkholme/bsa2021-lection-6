﻿using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Projects.API.Interfaces
{
    public interface IQueryProcessingService
    {
        Task<IEnumerable<OldestUsersInfo>> GetOldestTeamsAsync(CancellationToken token);

        Task<IEnumerable<ProjectInfo>> GetProjectsInfoAsync(CancellationToken token);

        Task<IEnumerable<KeyValuePair<UserEntity, List<TaskEntity>>>> GetTasksPerPerformerAlphabeticallyAsync(CancellationToken token);

        Task<IList<TaskEntity>> GetTasksPerPerformerAsync(int performerId, CancellationToken token);

        Task<IList<TaskInfo>> GetTasksPerPerformerFinishedThisYearAsync(int performerId, CancellationToken token);

        Task<IEnumerable<KeyValuePair<ProjectEntity, int>>> GetTasksQuantityPerProjectAsync(int userId, CancellationToken token);

        Task<UserInfo> GetUserInfoAsync(int userId, CancellationToken token);
        System.Threading.Tasks.Task<IEnumerable<TaskEntity>> GetUnhandledTasksForUser(int id, CancellationToken token);
    }
}