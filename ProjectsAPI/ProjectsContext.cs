﻿using Microsoft.EntityFrameworkCore;
using Projects.API.ExtensionsConfiguration;
using Projects.Modelling.Entities;

namespace Projects.API
{
    public class ProjectsContext : DbContext
	{
		public ProjectsContext(DbContextOptions<ProjectsContext> options)
			: base(options)
		{ }

		public DbSet<ProjectEntity> Projects { get; set; }

		public DbSet<TeamEntity> Teams { get; set; }

		public DbSet<TaskEntity> Tasks { get; set; }

		public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.Entity<TeamEntity>()
			.HasMany(t => t.Users)
			.WithOne(u => u.TeamEntity)
			.HasForeignKey(u => u.TeamEntityId)
			.OnDelete(DeleteBehavior.SetNull);

			modelBuilder.Entity<TeamEntity>()
				.HasMany(t => t.Projects)
				.WithOne(p => p.TeamEntity)
				.HasForeignKey(p => p.TeamEntityId)
				.OnDelete(DeleteBehavior.SetNull);

			modelBuilder.Entity<UserEntity>()
				.HasMany(u => u.Tasks)
				.WithOne(t => t.UserEntity)
				.HasForeignKey(t => t.UserEntityId)
				.OnDelete(DeleteBehavior.SetNull);

			modelBuilder.Entity<TaskEntity>()
				.HasOne(t => t.UserEntity)
				.WithMany(u => u.Tasks)
				.HasForeignKey(u => u.UserEntityId)
				.OnDelete(DeleteBehavior.SetNull);

			modelBuilder.Entity<ProjectEntity>()
				.HasMany(p => p.Tasks)
				.WithOne(t => t.ProjectEntity)
				.HasForeignKey(t => t.ProjectEntityId)
				.OnDelete(DeleteBehavior.SetNull);

			modelBuilder.Entity<ProjectEntity>()
				.HasOne(p => p.UserEntity)
				.WithMany(u => u.Projects)
				.HasForeignKey(u => u.UserEntityId)
				.OnDelete(DeleteBehavior.SetNull);
		}
    }
}