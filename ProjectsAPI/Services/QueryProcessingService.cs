﻿using Projects.API.Interfaces;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;

using ProjectsAPI.Extensions_and_Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Projects.API.Services
{
    public class QueryProcessingService : IQueryProcessingService
    {
        private readonly IEntityHandlerService handler;

        public QueryProcessingService(IEntityHandlerService handler)
        {
            this.handler = handler;
        }

        public async Task<IEnumerable<KeyValuePair<ProjectEntity, int>>> GetTasksQuantityPerProjectAsync(int userId, CancellationToken token)
        {
            var projects = await handler.GetAllProjectEntitiesAsync(token);

            var tasksQuantityPerProject = new List<KeyValuePair<ProjectEntity, int>>();

            tasksQuantityPerProject.AddRange(
                                    projects
                                    .WithCancellation(token)
                                    .Where(n => n.UserEntity.Id == userId)
                                    .Select(n => new KeyValuePair<ProjectEntity, int>(n, n.Tasks.ToList().Count))
                                    ?? new List<KeyValuePair<ProjectEntity, int>>());

            return tasksQuantityPerProject;
        }

        public async Task<IList<TaskEntity>> GetTasksPerPerformerAsync(int performerId, CancellationToken token)
        {
            var tasks = await handler.GetAllTaskEntitiesAsync(token);

            var tasksperPerPerformer = new List<TaskEntity>();

            try
            {
                tasksperPerPerformer.AddRange(
                tasks
                .WithCancellation(token)
                  .Where(n => n.UserEntity.Id == performerId)
                  .Where(n => n.Name.Count() < 45)
                  ?? new List<TaskEntity>());

                return tasksperPerPerformer;
            }
            catch { throw; }
        }

        public async Task<IList<TaskInfo>> GetTasksPerPerformerFinishedThisYearAsync(int performerId, CancellationToken token)
        {
            var tasks = await handler.GetAllTaskEntitiesAsync(token);

            var tasksperPerformer = new List<TaskInfo>();

            tasksperPerformer.AddRange(
                tasks
                .WithCancellation(token)
                .Where(n => n.UserEntity.Id == performerId)
                .Where(n => n.State == State.Finished)
                .Where(n => n.FinishedAt?.Year == DateTime.Now.Year)
                .Select(n => new TaskInfo() { Id = n.Id, Name = n.Name })
                ?? new List<TaskInfo>());

            return tasksperPerformer;
        }

        public async Task<IEnumerable<OldestUsersInfo>> GetOldestTeamsAsync(CancellationToken token)
        {
            var teams = await handler.GetAllTeamEntitiesAsync(token);

            var oldestUsers = new List<OldestUsersInfo>();

            oldestUsers.AddRange(
                teams
                .WithCancellation(token)
                .Where(team => team.Users.All(user => (DateTime.Now.Year - user.BirthDay.Year) > 10))
                .Select(n => new OldestUsersInfo()
                {
                    Id = n.Id,
                    Name = n.Name,
                    Users = n.Users
                             .OrderByDescending(user => user.RegisteredAt)
                })
                ?? new List<OldestUsersInfo>());

            return oldestUsers;
        }

        public async Task<IEnumerable<KeyValuePair<UserEntity, List<TaskEntity>>>> GetTasksPerPerformerAlphabeticallyAsync(CancellationToken token)
        {
            var tasks = await handler.GetAllTaskEntitiesAsync(token);

            var tasksPerPerformer = new List<KeyValuePair<UserEntity, List<TaskEntity>>>();

            tasksPerPerformer.AddRange(
                tasks
                  .WithCancellation(token)
                  .OrderByDescending(task => task.Name.Length)
                  .GroupBy(task => task.UserEntity)
                  .OrderBy(n => n.Key.FirstName)

                  .Select(n => new KeyValuePair<UserEntity, List<TaskEntity>>(n.Key, n.ToList()))
                  ?? new List<KeyValuePair<UserEntity, List<TaskEntity>>> ());

            return tasksPerPerformer;
        }

        public async Task<UserInfo> GetUserInfoAsync(int userId, CancellationToken token)
        {
            var user = await
                handler.GetUserEntitybyIdAsync(userId, token);

            try
            {
                return
                    new UserInfo()
                    {
                        User = user,
                        LastProject = user.Projects
                                          .DefaultIfEmpty()
                                          .Aggregate((a, b) => b.CreatedAt > a.CreatedAt ? b : a),

                        LastProjectTasksQuantity = user.Projects
                                                       .DefaultIfEmpty()?
                                                       .Aggregate((a, b) => b.CreatedAt > a.CreatedAt ? b : a)?
                                                       .Tasks?
                                                       .Count(),

                        UnhandledTasksQuantity = user.Tasks
                                                     .Where(n => n.State == State.Canceled || n.State == State.InProgress)
                                                     .Count(),

                        LongetsTask = user?.Tasks
                                          .DefaultIfEmpty()
                                          .Aggregate((a, b) =>
                                          {
                                              if (!b.FinishedAt.HasValue)
                                              {
                                                  if (a.FinishedAt.HasValue)
                                                      return b;

                                                  else
                                                      return b.CreatedAt < a.CreatedAt ? b : a;
                                              }
                                              else
                                              {
                                                  if (!a.FinishedAt.HasValue)
                                                      return b.CreatedAt < a.CreatedAt ? b : a;

                                                  else
                                                      return (b.FinishedAt - b.CreatedAt).Value > (a.FinishedAt - a.CreatedAt).Value ? b : a;
                                              }
                                          })
                    };
            }
            catch { throw; }
        }

        public async Task<IEnumerable<ProjectInfo>> GetProjectsInfoAsync(CancellationToken token)
        {
            var projects = await handler.GetAllProjectEntitiesAsync(token);

            var projectsInfo = new List<ProjectInfo>();

            projectsInfo.AddRange(
                projects
                .WithCancellation(token)
                .Select(prj => new ProjectInfo()
                {
                    Project = prj,
                    LongestTask = prj.Tasks.Count() > 0 ?
                                  prj.Tasks.DefaultIfEmpty().Aggregate((a, b) => b.Description.Length > a.Description.Length ? b : a) :
                                  null,
                    ShortestTask = prj.Tasks.Count() > 0 ?
                                   prj.Tasks.DefaultIfEmpty().Aggregate((a, b) => b.Name.Length < a.Name.Length ? b : a) :
                                   null,
                    UsersQuantity = (prj.Description.Length > 20 || prj.Tasks.Count() < 3) ?
                                                              prj.TeamEntity.Users.Count() :
                                                              0
                })
                ?? new List<ProjectInfo>());

            return projectsInfo;
        }

        public async Task<IEnumerable<TaskEntity>> GetUnhandledTasksForUser(int id, CancellationToken token)
        {
            var user = await handler.GetUserEntitybyIdAsync(id, token);

            var unhandledTasks = new List<TaskEntity>();
            
            unhandledTasks.AddRange(
                user?.Tasks?
                     .WithCancellation(token)
                     .Where(n => n.State != State.Finished)
                                     ?? new List<TaskEntity>());

            return unhandledTasks;
        }
    }
}
