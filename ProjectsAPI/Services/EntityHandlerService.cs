﻿using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.Modelling.Entities;
using Projects.Modelling.Interfaces;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projects.API.Interfaces;
using Projects.Modelling.DTOs;
using System.Threading;

namespace Projects.API.Services
{
    public class EntityHandlerService : IEntityHandlerService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IEntityBinderService binder;

        public EntityHandlerService(IUnitOfWork unitOfWork, IEntityBinderService binder)
        {
            this.unitOfWork = unitOfWork;
            this.binder = binder;
        }

        public async Task<IEnumerable<ProjectEntity>> GetAllProjectEntitiesAsync(CancellationToken token)
        {
            var projectModels = await
                (unitOfWork.Projects as ProjectRepository).QueryAsync(
                                                               n => true,
                                                               token,
                                                               n => n.Tasks,
                                                               n => n.TeamEntity,
                                                               n => n.UserEntity);

            return projectModels;
        }

        public async Task<ProjectEntity> GetProjectEntitybyIdAsync(int id, CancellationToken token)
        {
            var projectEntity = await
                (unitOfWork.Projects as ProjectRepository).GetByIdAsync(id,
                                          token,
                                          n => n.Tasks,
                                          n => n.TeamEntity,
                                          n => n.UserEntity);

            return projectEntity;
        }

        public async Task<IEnumerable<TaskEntity>> GetAllTaskEntitiesAsync(CancellationToken token)
        {
            var tasksEntities = await
                (unitOfWork.Tasks as TaskRepository).QueryAsync(
                                                         n => true,
                                                         token,
                                                         n => n.UserEntity,
                                                         n => n.ProjectEntity);

            return tasksEntities;
        }

        public async Task<TaskEntity> GetTaskEntitybyIdAsync(int id, CancellationToken token)
        {
            var taskEntity = await 
                (unitOfWork.Tasks as TaskRepository).GetByIdAsync(id,
                                       token,
                                       n => n.UserEntity,
                                       n => n.ProjectEntity);

            return taskEntity;
        }

        public async Task<IEnumerable<UserEntity>> GetAllUserEntitiesAsync(CancellationToken token)
        {
            var userEntities = await
                (unitOfWork.Users as UserRepository).QueryAsync(
                                                         n => true,
                                                         token,
                                                         n => n.Projects,
                                                         n => n.Tasks,
                                                         n => n.TeamEntity);

            return userEntities;
        }

        public async Task<UserEntity> GetUserEntitybyIdAsync(int id, CancellationToken token)
        {
            var userEntity = await
                (unitOfWork.Users as UserRepository).GetByIdAsync(
                                       id,
                                       token,                        
                                       n => n.Projects,
                                       n => n.Tasks,
                                       n => n.TeamEntity);

            return userEntity;
        }

        public async Task<IEnumerable<TeamEntity>> GetAllTeamEntitiesAsync(CancellationToken token)
        {
            var teamEntities = await
                (unitOfWork.Teams as TeamRepository).QueryAsync(
                                                          n => true,
                                                          token,
                                                          n => n.Users,
                                                          n => n.Projects);

            return teamEntities;
        }

        public async Task<TeamEntity> GetTeamEntitybyIdAsync(int id, CancellationToken token)
        {
            var teamEntity = await
                (unitOfWork.Teams as TeamRepository).GetByIdAsync(id,
                                       token,
                                       n => n.Users,
                                       n => n.Projects);

            return teamEntity;
        }
    }
}
